#!/bin/sh
### BEGIN INIT INFO
# Provides:          hopm
# Required-Start:    $remote_fs
# Required-Stop:     $remote_fs
# Should-Start:      hopm
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Hybrid Open Proxy Monitor
### END INIT INFO

PATH=/sbin:/bin:/usr/sbin:/usr/bin
DAEMON=/usr/bin/hopm
NAME=hopm
DESC="hopm scanner"
USER="irc"
GROUP="irc"
PIDDIR=/run/hopm
PIDFILE=$PIDDIR/hopm.pid

test -f $DAEMON || exit 0

. /lib/lsb/init-functions

# Source defaults file; edit that file to configure this script.
if [ -e /etc/default/hopm ]; then
  . /etc/default/hopm
fi


case "$1" in
  start)
	if [ ! -d $PIDDIR ]; then
		mkdir -p $PIDDIR
		chown $USER:$USER $PIDDIR
	fi
	log_daemon_msg "Starting $DESC" "$NAME"
	start-stop-daemon --start --quiet --user $USER --chuid $USER:$GROUP --pidfile $PIDFILE --exec $DAEMON
	log_end_msg $?
	;;
  stop)
	log_daemon_msg "Stopping $DESC" "$NAME"
	start-stop-daemon --stop --quiet --user $USER --pidfile $PIDFILE --exec $DAEMON
	log_end_msg $?
	;;
  restart|force-reload)
	$0 stop
	sleep 2
	$0 start
	;;
  reload|force-reload)
	log_daemon_msg "Reloading $DESC" "$NAME"
	start-stop-daemon --stop --quiet --user $USER --signal HUP --pidfile $PIDFILE
	log_end_msg $?
	;;
  status)
	status_of_proc "$DAEMON" "$NAME" && exit 0 || exit $?
	;;
  *)
	N=/etc/init.d/$NAME
	echo "Usage: $N {start|stop|restart|force-reload|status}" >&2
	exit 1
	;;
esac

exit 0
